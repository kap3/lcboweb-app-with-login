package sheridan;

/**
 * 
 * @author Seby
 *
 */

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		
		return loginName.length() > 5;
	}
	
	public static boolean isValidAlphaNumeric( String loginName ) {
		return loginName.matches("^[a-zA-Z0-9]*$");
	}
	
	public static boolean isValidNotStartingWithNumber( String loginName ) {
		char c = loginName.charAt(0);
		
		return !Character.isDigit(c);
	}
	
}
