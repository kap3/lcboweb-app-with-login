package sheridan;

/**
 * author seby
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramsestrejos12" ) );
	}
	
	@Test
	public void testIsValidLoginExeption( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "a" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}
	
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		assertFalse("Valid login" , LoginValidator.isValidLoginName( "ramse" ) );
	}
	
	@Test
	public void testIsValidAlphaNumericCharactersOnlyRegular() {
		assertTrue("Invalid login", LoginValidator.isValidAlphaNumeric("ramses12345"));
	}
	
	@Test
	public void testIsValidAlphaNumericCharactersOnlyException() {
		assertFalse("Valid login", LoginValidator.isValidAlphaNumeric("ram--d00--!"));
	}
	
	@Test
	public void testIsValidAlphaNumericCharactersOnlyBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.isValidAlphaNumeric("ramses123"));
	}
	
	@Test
	public void testIsValidAlphaNumericCharactersOnlyBoundaryOut() {
		assertFalse("Valid login", LoginValidator.isValidAlphaNumeric("ramses!"));
	}
	
	@Test
	public void testIsValidNotStartingWithNumberRegular() {
		assertTrue("Invalid Login", LoginValidator.isValidNotStartingWithNumber("ramses"));
	}
	
	@Test
	public void testIsValidNotStartingWithNumberException() {
		assertFalse("Valid Login", LoginValidator.isValidNotStartingWithNumber("1ramses"));
	}
	
	@Test
	public void testIsValidNotStartingWithNumberBoundaryIn() {
		assertTrue("Invalid Login", LoginValidator.isValidNotStartingWithNumber("r1amses"));
	}
	
	@Test
	public void testIsValidNotStartingWithNumberBoundaryOut() {
		assertFalse("Valid Login", LoginValidator.isValidNotStartingWithNumber("111ramses"));
	}
	

	

}
